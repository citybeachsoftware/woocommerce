<?php
/**
 * Connect page details.
 * Version: 3.5
 * Author: eBay Australia
 * Author URI: https://ebay.com.au
 */

$adminClass = 'eBayLinkAdmin';

if (!class_exists($adminClass)) {
  wp_die(__('Plugin Administation class is missing.'));
}

$citybeach_key = $adminClass::connection_key();

$registered_connection_key = get_option($adminClass::flavour . '_connection_key');
$registered_connection_key_has = !empty($registered_connection_key);
$registered_connection_key = $registered_connection_key_has ? $registered_connection_key : $adminClass::generate_key();

$registered_connection_endpoint = get_option($adminClass::flavour . '_connection_endpoint');
$registered_connection_endpoint_has = !empty($registered_connection_endpoint);
$registered_connection_endpoint = $registered_connection_endpoint_has ? $registered_connection_endpoint : get_rest_url();

$registered_connection_email = get_option($adminClass::flavour . '_connection_email');
$registered_connection_email_has = !empty($registered_connection_email);
$registered_connection_email = $registered_connection_email_has ? $registered_connection_email : get_bloginfo('admin_email');

$registered_connection_name = get_option($adminClass::flavour . '_connection_name');
$registered_connection_name_has = !empty($registered_connection_name);
$registered_connection_name = $registered_connection_name_has ? $registered_connection_name : get_bloginfo('name');

$registered_connection_saved = $registered_connection_key_has && $registered_connection_endpoint_has && $registered_connection_email_has && $registered_connection_name_has;
?>

<h2><?php echo $adminClass::flavourLabel; ?></h2>

<div class="wrap">
  <?php if ($registered_connection_saved): ?>
      <a class="button button-primary" href="<?php menu_page_url( $adminClass::flavour . '_connect' ); ?>">Go To <?php echo $adminClass::flavourLabel; ?></a>
  <?php else: ?>
      <p>You must save your connection details before your first connection to <?php echo $adminClass::flavourLabel; ?>.</p>
  <?php endif; ?>

    <h2>Connection Settings</h2>
    <form method="post" action="options.php">
      <?php settings_fields( $adminClass::flavour . '-connection' ); ?>
      <?php do_settings_sections( $adminClass::flavour . '-connection' ); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Store Name</th>
                <td><input type="text" name="<?php echo $adminClass::flavour; ?>_connection_name" <?php if ($registered_connection_name_has): ?>disabled <?php endif; ?>value="<?php echo $registered_connection_name; ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Email address</th>
                <td><input type="text" name="<?php echo $adminClass::flavour; ?>_connection_email" <?php if ($registered_connection_email_has): ?>disabled <?php endif; ?>value="<?php echo $registered_connection_email; ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Account Key</th>
                <td><input type="text" name="<?php echo $adminClass::flavour; ?>_connection_key" <?php if ($registered_connection_key_has): ?>disabled <?php endif; ?>value="<?php echo $registered_connection_key; ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">API endpoint</th>
                <td><input type="text" name="<?php echo $adminClass::flavour; ?>_connection_endpoint" <?php if ($registered_connection_endpoint_has): ?>disabled <?php endif; ?>value="<?php echo $registered_connection_endpoint; ?>" /></td>
            </tr>
        </table>

      <?php if (!$registered_connection_saved): ?>
        <?php submit_button(); ?>
      <?php endif; ?>
    </form>
</div>
