<?php
/**
 * Plugin Name: eBayLink
 * Description: Connect your WooCommerce to eBay Australia
 * Version: 3.5
 * Author: eBay Australia
 * Author URI: https://ebay.com.au
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))) ||
  in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', array_keys(get_site_option('active_sitewide_plugins'))))) {

  if (!class_exists('eBayLinkAdmin')) {

    class eBayLinkAdmin {
      const baseUrl = 'https://ebaylink.omnivore.com.au/wooCommerce/connect';
      const flavour = 'ebaylink';
      const flavourLabel = 'eBayLink';
      const keyName = 'CityBeach eBayLink';


      public function initAdminMenu() {
        add_menu_page(self::flavourLabel, self::flavourLabel, 'manage_options', self::flavour, array($this, 'dashboard'), null, '59');
        add_submenu_page( null, null, null, 'manage_options', self::flavour . '_connect', array($this, 'connect'));
        add_action( 'admin_init', array($this, 'settings') );
      }


      public function settings() {
        register_setting( self::flavour . '-connection', self::flavour . '_connection_key', array($this, 'settings_key_validate') );
        register_setting( self::flavour . '-connection', self::flavour . '_connection_endpoint', array($this, 'settings_endpoint_validate') );
        register_setting( self::flavour . '-connection', self::flavour . '_connection_email', array($this, 'settings_email_validate') );
        register_setting( self::flavour . '-connection', self::flavour . '_connection_name', array($this, 'settings_name_validate') );
      }


      private function create_update_api_key() {
        global $wpdb;

        // Remove any existing key.
        $wpdb->delete( $wpdb->prefix . 'woocommerce_api_keys', array( 'description' => self::keyName ), array( '%s' ) );

        $user = wp_get_current_user();

        // Created API keys.
        $permissions     = 'read_write';
        $consumer_key    = 'ck_' . wc_rand_hash();
        $consumer_secret = 'cs_' . wc_rand_hash();

        $detail = array(
          'user_id'         => $user->ID,
          'description'     => self::keyName,
          'permissions'     => $permissions,
          'consumer_key'    => wc_api_hash( $consumer_key ),
          'consumer_secret' => $consumer_secret,
          'truncated_key'   => substr( $consumer_key, -7 ),
        );

        $wpdb->insert($wpdb->prefix . 'woocommerce_api_keys', $detail, array('%d', '%s', '%s', '%s', '%s', '%s',));

        $detail['consumer_key'] = $consumer_key;

        return $detail;
      }


      private static function woocommerce_version() {
        if ( class_exists( 'WooCommerce' ) ) {
          global $woocommerce;
          return $woocommerce->version;
        }
        else {
          return false;
        }
      }


      public static function generate_key() {
        $length = 128;
        $keyspace = '0123456789abcdefghijklmnopqrstuvwxyz';
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
          $str .= $keyspace[mt_rand(0, $max)];
        }
        return $str;
      }


      public static function connection_key () {
        global $wpdb;

        $row = $wpdb->get_row(
          $wpdb->prepare(
            "SELECT consumer_key, consumer_secret FROM {$wpdb->prefix}woocommerce_api_keys WHERE description = %s",
            self::keyName
          )
        );

        return $row;
      }


      public function settings_key_validate($input) {
        $existing = get_option(self::flavour . '_connection_key');
        if (!empty($existing)) {
          return $existing;
        }
        else {
          $newinput = trim($input);

          if(!preg_match('/^[a-zA-Z0-9]{128}$/', $newinput)) {

            $newinput = '';
          }
        }
        return $newinput;
      }


      public function settings_endpoint_validate($input) {
        $existing = get_option(self::flavour . '_connection_endpoint');
        if (!empty($existing)) {
          return $existing;
        }
        else {
          $newinput = trim($input);

          if(!filter_var($newinput, FILTER_VALIDATE_URL)) {
            $newinput = '';
          }
        }
        return $newinput;
      }


      public function settings_email_validate($input) {
        $existing = get_option(self::flavour . '_connection_email');
        if (!empty($existing)) {
          return $existing;
        }
        else {
          $newinput = trim($input);

          if(!is_email($newinput)) {
            $newinput = '';
          }
        }
        return $newinput;
      }


      public function settings_name_validate($input) {
        $existing = get_option(self::flavour . '_connection_name');
        if (!empty($existing)) {
          return $existing;
        }
        else {
          $newinput = trim($input);
        }
        return $newinput;
      }


      public function dashboard() {
        if (!current_user_can( 'manage_options'))  {
          wp_die(__('You do not have sufficient permissions to access this page.'));
        }
        require_once(__DIR__ . '/connect.php');
      }


      public function connect() {
        if (!current_user_can( 'manage_options'))  {
          wp_die(__('You do not have sufficient permissions to access this page.'));
        }

        $key_details = $this->create_update_api_key();
        $connection_key = get_option(self::flavour . '_connection_key');
        $connection_endpoint = get_option(self::flavour . '_connection_endpoint');
        $connection_email = get_option(self::flavour . '_connection_email');
        $connection_name = get_option(self::flavour . '_connection_name');

        $url = self::baseUrl
          . '?' . 'flavour=' . urlencode(self::flavour)
          . '&' . 'storeName=' . urlencode($connection_name)
          . '&' . 'email=' . urlencode($connection_email)
          . '&' . 'endpoint=' . urlencode($connection_endpoint)
          . '&' . 'key=' . urlencode($connection_key)
          . '&' . 'wordpress_version=' . urlencode(get_bloginfo('version'))
          . '&' . 'woocommerce_version=' . urlencode(self::woocommerce_version())
          . '&' . 'consumer_key=' . urlencode($key_details['consumer_key'])
          . '&' . 'truncated_key=' . urlencode($key_details['truncated_key'])
          . '&' . 'consumer_secret=' . urlencode($key_details['consumer_secret']);

        wp_redirect( $url );
      }
    }
  }

  if (!class_exists('eBayLink')) {

    class eBayLink {
      protected $tag = 'ebaylink';
      protected $name = ' eBayLink';
      protected $version = '3.0';
      protected $notice = '';

      public function getVersion() {
        return $this->version;
      }

      public function lateLoad() {
        global $pagenow;

        include_once(ABSPATH . 'wp-includes/pluggable.php');

        if ($pagenow === 'admin.php' || $pagenow === "admin-ajax.php") {
          @ob_start();
        }

        $admin = new eBayLinkAdmin();

        add_action('admin_menu', array($admin, 'initAdminMenu'));
      }

      public function __construct() {
        add_action('plugins_loaded', array($this, 'lateLoad'));
      }
    }

    global $ebaylink;
    $ebaylink = new eBayLink();
  }
}
