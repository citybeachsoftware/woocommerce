=== eBay Link ===
Tags: ebay, woocommerce
License: Custom
License URI: http://www.omnivore.com.au/index.php/ebaylink-terms-of-use/
Contributors: ebaylink, citybeachsoftware
Requires at least: 4.1.1
Tested up to: 5.0.2
Stable tag: trunk
Requires PHP: 5.6

WooCommerce to eBay Australia integration.

== Description ==

= About eBay LINK =

eBay LINK is an extension for WooCommerce that makes selling on eBay Australia easy.

eBay LINK creates listings on eBay Australia using your products in WooCommerce then orders are sent back to your WooCommerce store for fulfilment.

eBay LINK will keep your listings including inventory and orders on eBay in sync with WooCommerce reducing any manual work or double handling.

= How it works =

* eBay LINK imports products from your WooCommerce store
* Once products are in eBay LINK, you can make changes to the price, product title and description without changing them in WooCommerce so that they are optimised for selling on eBay
* eBay LINK creates the listings on eBay
* eBay LINK keeps eBay and WooCommerce in sync with nightly (or as scheduled) product and inventory syncs
* Orders from eBay are sent back to WooCommerce for fulfilment.

= Additional Features =

* Make changes to price and product titles to optimise your products to sell on eBay
* Add a stock buffer to reduce the chance of overselling
* Choose eBay categories for your products to list in
* Block products you don’t want to sell on eBay - single products or exclude by keyword, brand
* Create product groups for shipping so you can have different shipping charges for different products. You can even do weight based shipping and eBay LINK supports Business Policies in eBay

= How to get set up =

* Install the eBay LINK Extension and follow the registration flow to link eBay Link with eBay and set up your product listing template, shipping and returns policies.
* You will need an eBay.com.au account and a PayPal account. Its recommended you subscribe to an eBay store to save on eBay listing fees but it is not mandatory.
* Once you can see the Dashboard in eBay LINK and eBay LINK has finished importing your products, you can choose which ones you want to list on eBay by choosing categories for them.

PLEASE NOTE: If you have existing listings on eBay, they will be removed and you will lose your sales history/people watching the listings. Reviews will remain. Please contact us before you install the extension if you have questions about this.


== Changelog ==

= 3.5 =
* Initial public release.

== Upgrade Notice ==

= 3.5 =
* Initial public release.
