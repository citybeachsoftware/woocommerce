<?php
/**
 * Runs on Uninstall.
 * Version: 3.5
 * Author: Catch
 * Author URI: https://catch.com.au/
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Check that we should be doing this
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
  exit; // Exit if accessed directly
}

global $wpdb;

$flavour = 'catchfeeder';
$key_name = 'CityBeach CatchFeeder';

// Delete Options
$options = array(
  $flavour . '_connection_key',
  $flavour . '_connection_endpoint',
  $flavour . '_connection_email',
  $flavour . '_connection_name',
);

foreach ( $options as $option ) {
  if ( get_option( $option ) ) {
    delete_option( $option );
  }
}

// Remove REST API key.
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))) ||
  in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', array_keys(get_site_option('active_sitewide_plugins'))))) {
  $wpdb->delete( $wpdb->prefix . 'woocommerce_api_keys', array( 'description' => $key_name ), array( '%s' ) );
}

